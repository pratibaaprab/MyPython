import pandas as pd
from pandas_datareader import wb
from datetime import datetime
from pandas_datareader.nasdaq_trader import get_nasdaq_symbols


#Importing CSV Data manually:
disney_prices = pd.read_csv('disney_prices.csv')

#Importing Data Using Datareader:
start = datetime(2005, 1, 1)
end = datetime(2008, 1, 1)
indicator_id = 'NY.GDP.PCAP.KD'

gdp_per_capita = wb.download(indicator=indicator_id, start=start, end=end, country=['US', 'CA', 'MX'])

print(gdp_per_capita)

#Get NASDAQ Symbols:
print(symbols = get_nasdaq_symbols())

#Filtering Data by Date
start = datetime(2019,1,1)
end = datetime(2019,2,1)
sap_data = web.DataReader('SP500', 'fred', start, end)
print(sap_data)

#API Keys:
dr.get_data_tiingo('GOOG', api_key='my-api-key')

#Using the Shift Operation:
                   
start = datetime(2008, 1, 1)
end = datetime(2018, 1, 1)

gdp = web.DataReader('GDP', 'fred', start, end)
gdp['growth'] = gdp['GDP'] - gdp['GDP'].shift(1)
print(gdp)

#Calculating Basic Financial Stats
tsp_data = pd.read_csv("tsp_data.csv", header = 0, index_col = 0).var()

print(tsp_data)
print(tsp_data.var())
print(tsp_data.cov())

#########
apple_prices = pd.read_csv('apple_prices.csv')
apple_prices['open'].var()

start = (2008,1,1)
end = (2018,1,1)
gas_prices = web.DataReader('APUS12A74714', 'fred', start, end)
print(gas_prices)