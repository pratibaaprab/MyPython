import pandas as pd

ad_clicks = pd.read_csv('ad_clicks.csv')
ad_clicks.head()
print(ad_clicks)

x = ad_clicks.groupby('utm_source').user_id.count().reset_index()

print(x)

ad_clicks['is_click'] = ~ad_clicks\
   .ad_click_timestamp.isnull()

clicks_by_source = ad_clicks.groupby(['utm_source', 'is_click'])['user_id'].count().reset_index()

print(clicks_by_source)

clicks_pivot = clicks_by_source.pivot(
  columns = 'is_click',
  index = 'utm_source',
  values = 'user_id').reset_index()

clicks_pivot['percent_clicks'] = clicks_pivot[True] / (clicks_pivot[True] + clicks_pivot[False])

print(clicks_pivot)

x = ad_clicks.groupby('experimental_group').user_id.count().reset_index()
print(x)

print(ad_clicks.groupby(['experimental_group','is_click'])['user_id'].count().reset_index())

a_clicks = ad_clicks[ad_clicks.experimental_group == "A"]
print(a_clicks)
 
b_clicks = ad_clicks[ad_clicks.experimental_group == "B"]
print(b_clicks)

print(a_clicks['percent'] = a_clicks[True] / (a_clicks[True] + a_clicks[False]))

print(b_clicks['percent'] = b_clicks[True] / (b_clicks[True]+ b_clicks[False]))