import pandas as pd

#Adding a column:
df = pd.DataFrame([
  [1, '3 inch screw', 0.5, 0.75],
  [2, '2 inch nail', 0.10, 0.25],
  [3, 'hammer', 3.00, 5.50],
  [4, 'screwdriver', 2.50, 3.00]
],
  columns=['Product ID', 'Description', 'Cost to Manufacture', 'Price']
)
df['Sold in Bulk?'] = ['Yes', 'Yes', 'No', 'No']


print(df)

###
df = pd.DataFrame([
  [1, '3 inch screw', 0.5, 0.75],
  [2, '2 inch nail', 0.10, 0.25],
  [3, 'hammer', 3.00, 5.50],
  [4, 'screwdriver', 2.50, 3.00]
],
  columns=['Product ID', 'Description', 'Cost to Manufacture', 'Price']
)

df['Is taxed?'] = ['Yes', 'Yes', 'Yes', 'Yes']


print(df)

###
df = pd.DataFrame([
  [1, '3 inch screw', 0.5, 0.75],
  [2, '2 inch nail', 0.10, 0.25],
  [3, 'hammer', 3.00, 5.50],
  [4, 'screwdriver', 2.50, 3.00]
],
  columns=['Product ID', 'Description', 'Cost to Manufacture', 'Price']
)
df['Margin'] = df['Price'] - df['Cost to Manufacture']
# Add column here

print(df)

#Performing Column Operations:
df = pd.DataFrame([
  ['JOHN SMITH', 'john.smith@gmail.com'],
  ['Jane Doe', 'jdoe@yahoo.com'],
  ['joe schmo', 'joeschmo@hotmail.com']
],
columns=['Name', 'Email'])

df['Lowercase Name'] = df.Name.apply(str.lower)


print(df)

#Lambda Function:
mylambda = lambda x : x[0]+x[-1]
print(mylambda("Hello there my child"))

###
mylambda = lambda x: 'Welcome to BattleCity!' \
  if x >= 13 \
  else 'You must be 13 or older'

#Applying a Lambda to a Column:
df = pd.read_csv('employees.csv')
df['Last Name'] = df.name.apply(
  lambda get_last_name: get_last_name.split(' ')[-1]
)
print(df)

#Applying a Lambda to a Row:
df = pd.read_csv('employees.csv')
print(df)

total_earned = lambda row: (row.hourly_wage * 40) + ((row.hours_worked - 40) *(row.hoursly_wage*1.5)) \
  if row.hours_worked > 40 else row.hours_worked * row.hourly_wage

df['Total Earned'] = df.apply(total_earned, axis = 1)

#renaming Columns:
df = pd.read_csv('imdb.csv')

df.columns = ['ID', 'Title', 'Category', 'Year Released', 'Rating']

print(df)

###
df = pd.read_csv('imdb.csv')

df.rename(columns = {
  'name' : 'movie_title'},
  inplace = True
)

print(df)

