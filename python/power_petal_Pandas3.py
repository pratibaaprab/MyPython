import pandas as pd
inventory = pd.read_csv('inventory.csv')
print(inventory)

staten_island = inventory.head(10)


product_request = inventory['product_description']

seed_request = inventory[(inventory.product_type =='seeds') & (inventory.location =='Brooklyn')]

inventory['in_stock'] = inventory.apply(lambda x: True if x.quantity > 0 else False, axis=1)

inventory['total_value'] = inventory['price'] * inventory['quantity']

combine_lambda = lambda row: \
    '{} - {}'.format(row.product_type,
                     row.product_description)

inventory['full description'] = inventory.apply(combine_lambda, axis=1)


print(inventory)