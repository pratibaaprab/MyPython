import pandas as pd
import pandas_datareader.data as web
from datetime import datetime
import pandas_datareader.wb as wb
import numpy as np

gold_prices = pd.read_csv('gold_prices.csv')
print(gold_prices)

crude_oil_prices = pd.read_csv('crude_oil_prices.csv')
print(crude_oil_prices)

start = datetime(1999, 1, 1)
end = datetime(2019,1,1)

nasdaq_data = web.DataReader('NASDAQ100', 'fred', start, end)

print(nasdaq_data)

sap_data = web.DataReader('SP500', 'fred', start, end)
print(sap_data)

gdp_data = wb.download(indicator='NY.GDP.MKTP.CD', country=['US'], start=start, end=end)

export_data = wb.download(indicator='NE.EXP.GNFS.CN', country=['US'], start=start, end=end)
print(export_data)

def log_return(prices):
  prices / prices.shift(1)
  np.log(Series)

gold_returns = log_return(gold_prices)
print(gold_returns)
